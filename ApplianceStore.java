import java.util.Scanner;

public class ApplianceStore{
   public static void main(String[] args){
      Scanner scan = new Scanner(System.in);
      Microwave[] applianceArray = new Microwave[4];
         for (int i = 0; i < applianceArray.length; i++){
            applianceArray[i] = new Microwave();

            System.out.println("What dish you want to microwave?");
            applianceArray[i].food = scan.nextLine();

            System.out.println("What is your dish initial temperature?");
            applianceArray[i].temperature = scan.nextInt();

            System.out.println("For how long you want to microwave your food?");
            System.out.println("(Each second increase temperature by 3!)");
			applianceArray[i].time = scan.nextInt();

            scan.nextLine();
         }

      System.out.println();

      Microwave lastMicrowave = applianceArray[applianceArray.length-1];
      System.out.println("Food inside the last microwave is " + lastMicrowave.food);
      System.out.println("Intial temperature of the " + lastMicrowave.food + " is  " + lastMicrowave.temperature);
      System.out.println("Time to microwave is " + lastMicrowave.time + " seconds");
      
      System.out.println();

      Microwave firstMicrowave = applianceArray[0];
      System.out.println("Your " + firstMicrowave.food + " is heated up to " + firstMicrowave.heat() + " degrees celsius");
      System.out.println("Your " + firstMicrowave.food + " temperature is lowered to " + firstMicrowave.freeze() + " degrees celsius");
   }
}