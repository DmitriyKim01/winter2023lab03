public class Microwave{
   public String food;
   public double temperature;
   public int time;

   public double heat(){
      // For each second in the microwave, 
      // food temperature increasing by 3 degrees 
      double finalTemperature = time * 3 + temperature;
      return finalTemperature;
   }
   public double freeze(){
      // For each second in the microwave, 
      // food temperature decreasing by 3 degrees 
      double finalTemperature = temperature - time * 3; 
      return finalTemperature;
   }	
}